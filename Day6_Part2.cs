using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.IO;

namespace ConsoleApp1
{
    class Day6_Part2
    {
        public static int size = 400;
        public static char[] alphas = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".ToArray();

        public static List<Cell> assigned_coords = new List<Cell>();

        public static int GetDistance(int x1, int y1, int x2, int y2)
        {
            return Math.Abs(x1 - x2) + Math.Abs(y1 - y2);
        }

        public static int GetCellAtRowCol(int row, int col)
        {
            return row * size + col;
        }

        public class Cell
        {
            public int X;
            public int Y;
            public char C;

            public int Number;

            public Dictionary<char, int> DistanceFromPoint = new Dictionary<char, int>();

            public Cell(int x, int y, char c)
            {
                this.X = x;
                this.Y = y;
                this.C = c;

                this.Number = GetCellAtRowCol(y, x);
            }

            public bool IsWithinDistance(int max)
            {
                int total = 0;
                foreach (KeyValuePair<char, int> pair in this.DistanceFromPoint)
                {
                    total += pair.Value;
                }
                if (total < max) return true;
                return false;
            }
        }

        public static void Main()
        {
            WebClient client = new WebClient();
            Stream stream = client.OpenRead("https://pastebin.com/raw/iXPxB3WC");
            StreamReader reader = new StreamReader(stream);

            List<Cell> all_cells = new List<Cell>();

            for (int yy = 0; yy < size; yy++)
            {
                for (int xx = 0; xx < size; xx++)
                {
                    Cell cell = new Cell(xx, yy, '.');
                    all_cells.Add(cell);
                }
            }

            string line;

            int id = 0;

            while ((line = reader.ReadLine()) != null)
            {
                string[] split_string = line.Split(',');

                int x = int.Parse(split_string[0]);
                int y = int.Parse(split_string[1]);

                Cell cell = new Cell(x, y, alphas[id]);
                assigned_coords.Add(cell);

                id++;
            }

            foreach (Cell cell in all_cells)
            {
                Cell active_cell = assigned_coords.Find(c => c.Number == cell.Number);
                if (active_cell != null) cell.C = active_cell.C;
            }

            foreach (Cell cell in all_cells)
            {
                foreach (Cell target in assigned_coords)
                {
                    int distance = GetDistance(cell.X, cell.Y, target.X, target.Y);
                    cell.DistanceFromPoint.Add(target.C, distance);
                }
            }

            int total_size = 0;
            foreach (Cell c in all_cells)
            {
                if (c.IsWithinDistance(10000)) total_size++;
            }

            Console.Write("Total size of the area is : " + total_size);

            Console.ReadKey();
        }
    }
}
