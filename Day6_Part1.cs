using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.IO;

namespace ConsoleApp1
{
    class Day6_Part1
    {
        public static int size = 400;

        public class Cell
        {
            public int X;
            public int Y;
            public char C;

            public int Number;

            public Cell(int x, int y, char c)
            {
                this.X = x;
                this.Y = y;
                this.C = c;

                this.Number = GetCellAtRowCol(y, x);
            }

            public Dictionary<char, int> DistanceFromPoint = new Dictionary<char, int>();
        }

        public static int GetDistance(int x1, int y1, int x2, int y2)
        {
            return Math.Abs(x1 - x2) + Math.Abs(y1 - y2);
        }

        public static int GetCellAtRowCol(int row, int col)
        {
            return row * size + col;
        }

        public static int GetRow(int cell)
        {
            return cell / size;
        }

        public static int GetColumn(int cell)
        {
            return cell % size;
        }

        public static bool IsInfinite(int x, int y)
        {
            if (x-1 == -1 || x+1 == size) return true;
            else if (y-1 == -1 || y+1 == size) return true;
            return false;
        }

        public static void Main()
        {
            WebClient client = new WebClient();
            Stream stream = client.OpenRead("https://pastebin.com/raw/iXPxB3WC");
            StreamReader reader = new StreamReader(stream);

            char[] alphas = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".ToArray();

            List<Cell> all_cells = new List<Cell>();
            List<Cell> assigned_coords = new List<Cell>();


            for (int yy = 0; yy < size; yy++)
            {
                for (int xx = 0; xx < size; xx++)
                {
                    Cell cell = new Cell(xx, yy, '.');
                    all_cells.Add(cell);
                }
            }

            string line;

            int id = 0;

            while ((line = reader.ReadLine()) != null)
            {
                string[] split_string = line.Split(',');

                int x = int.Parse(split_string[0]);
                int y = int.Parse(split_string[1]);

                Cell cell = new Cell(x, y, alphas[id]);
                assigned_coords.Add(cell);

                id++;
            }

            foreach (Cell cell in all_cells)
            {
                Cell active_cell = assigned_coords.Find(c => c.Number == cell.Number);
                if (active_cell != null) cell.C = active_cell.C;
            }

            foreach (Cell cell in all_cells)
            {
                foreach (Cell target in assigned_coords)
                {
                    int distance = GetDistance(cell.X, cell.Y, target.X, target.Y);
                    cell.DistanceFromPoint.Add(target.C, distance);
                }
                
                int value = cell.DistanceFromPoint.Values.Min();

                int count = 0;
                foreach (KeyValuePair<char, int> pair in cell.DistanceFromPoint)
                {
                    if (pair.Value == value)
                    {
                        count++;
                    }
                }
                if (count > 1) cell.C = '@';
                else cell.C = cell.DistanceFromPoint.Aggregate((a, b) => a.Value < b.Value ? a : b).Key;
                
            }

            Dictionary<char, bool> FiniteAreas = new Dictionary<char, bool>();

            foreach (char c in alphas)
            {
                FiniteAreas.Add(c, true);
            }

            foreach (Cell cell in all_cells)
            {
                if (IsInfinite(cell.X, cell.Y))
                {
                    FiniteAreas[cell.C] = false;
                }
            }

            Dictionary<char, int> SizeByArea = new Dictionary<char, int>();

            foreach (KeyValuePair<char, bool> area in FiniteAreas)
            {
                if (area.Value)
                {
                    SizeByArea.Add(area.Key, 0);
                    foreach (Cell c in all_cells)
                    {
                        if (c.C == area.Key) SizeByArea[area.Key]++;
                    }
                }
            }

            int biggest_size = SizeByArea.Values.Max();
            char biggest_area = SizeByArea.Aggregate((a, b) => a.Value > b.Value ? a : b).Key;

            Console.Write(biggest_area + " is the biggest area, with a total of " + biggest_size + " cells!");

            Console.ReadKey();
        }
    }
}